.PHONY: help

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

install: ## prepare the example app
	docker compose build app
	docker compose run --rm --entrypoint composer --workdir /var/www/nette-blog app install --optimize-autoloader
	docker compose run --rm --entrypoint bash --workdir /var/www/nette-blog app -c 'rm -rf temp/cache && chmod 777 log temp'

run: ## run the app and nginx proxy on port 8000
	docker compose up

bombardier: ## run http stress test bombardier
	docker run --rm -it --net=host alpine/bombardier -m GET 'http://127.0.0.1:8000' -c 10 -d 1m