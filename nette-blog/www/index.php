<?php

declare(strict_types=1);

$phpStart = microtime(true);

require __DIR__ . '/../vendor/autoload.php';

App\Presenters\HomePresenter::$timestamps['php start'] = $phpStart;
App\Presenters\HomePresenter::$timestamps['vendor autoload'] = microtime(true);

$configurator = App\Bootstrap::boot();
$container = $configurator->createContainer();
$application = $container->getByType(Mallgroup\RoadRunner\RoadRunner::class);

App\Presenters\HomePresenter::$timestamps['nette boot'] = microtime(true);

$application->run();

$request = $container->getByType(Nette\Http\IRequest::class);
if ($request->isAjax()) {
    echo App\Presenters\HomePresenter::getTimeLine("php end", microtime(true));
}
