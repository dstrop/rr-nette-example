<?php
declare(strict_types=1);

namespace App\Middleware;

use App\Presenters\HomePresenter;
use Nette\DI\Container;
use Nette\Http\IRequest;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class TimerMiddleware implements MiddlewareInterface
{
    public function __construct(private Container $container)
    {
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        HomePresenter::$timestamps['php in'] = microtime(true);
        $response = $handler->handle($request);

        if ($this->container->getByType(IRequest::class)->isAjax()) {
            $newBodyStream = \GuzzleHttp\Psr7\Utils::streamFor(
                $response->getBody() .
                HomePresenter::getTimeLine('php out', microtime(true))
            );

            return $response->withBody($newBodyStream);
        }

        return $response;

    }
}
