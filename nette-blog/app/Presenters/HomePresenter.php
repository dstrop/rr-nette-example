<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use Tracy\Debugger;


final class HomePresenter extends Nette\Application\UI\Presenter
{
    public static int $requestTime;
    public static array $timestamps = [];

    public function actionDefault()
    {
        self::$timestamps['presenter'] = microtime(true);

        if ($this->isAjax()) {
            $this->getHttpResponse()->setContentType('text/plain');
            self::$requestTime = intval(floatval($this->getHttpRequest()->getHeader('x-request-time')) * 1000);
            $this->getHttpResponse()->setHeader('x-request-time', (string)self::$requestTime);

            $response = "ngix request time: {$this::$requestTime}\n";

            foreach (self::$timestamps as $k => $v) {
                $response .= self::getTimeLine($k, $v);
            }

            $this->sendResponse(new Nette\Application\Responses\TextResponse($response));
        }
    }

    public static function getTimeLine(string $point, float $timestamp): string
    {
        $time = intval($timestamp * 1000) - self::$requestTime;

        return "$point: $time\n";
    }
}
