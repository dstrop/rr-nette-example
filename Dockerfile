FROM composer:2 AS composer
FROM ghcr.io/roadrunner-server/roadrunner:2023.1.3 AS roadrunner
FROM php:8.2-cli

# Install dependencies
RUN apt-get update && \
    apt-get install -y \
        libzip-dev \
        zip \
        unzip && \
    docker-php-ext-install zip opcache sockets

# Copy Composer from the Composer image
COPY --from=composer /usr/bin/composer /usr/bin/composer

# Copy the custom opcache configuration file into the container
COPY ./opcache.ini $PHP_INI_DIR/conf.d/

COPY --from=roadrunner /usr/bin/rr /usr/local/bin/rr

ENTRYPOINT ["/var/www/nette-blog/entrypoint.sh"]
